import React from 'react';
import ReactDom from 'react';

class Statesexample extends React.Component
{
    constructor(props){
        super(props);
        this.state = {
            header:"Header from state...",
            content:"Content from state.."
        }



    }
   /* render(){
        return(
            <div>
               <h1>
                    {this.state.header}
                </h1>
                <h2>
                    {this.state.content}
                </h2> 
                
            </div>
        );
    }
}*/
render(){
    return(
        <div>
           <Header headerProp={this.state.header} />
           <Content contentprop={this.state.content}/>
            
        </div>
    );

}
}
class Header extends React.Component{
    render(){
        return(
            <div>
                <h1>{this.props.headerProp}</h1>
            </div>
        );
    }
}
class Content extends React.Component{
render(){
    return(
        <div>
            <h2>{this.props.contentprop}</h2>
        </div>
    );
}
}
export default Statesexample;