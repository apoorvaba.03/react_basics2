import React from 'react';
import ReactDOM from 'react';
class Childevent extends React.Component{
    constructor(props){
        super(props);
        this.state = {
            data : 'Intial Data...'
        }
        this.updateState = this.updateState.bind(this);
    }// end of constructor
    updateState(){
        this.setState(
            {
                data:'data updated from child componet'
            }
        )
    }
    render(){
        return(
            <div>
                <Content myDataProp = {this.state.data}
                    updateStateProp = {this.updateState} />

            </div>
        );// end of return
    }
}// end of class
class Content extends React.Component{
    render(){
        return(
            <div>
                <button onClick={this.props.updateStateProp}>Click</button>
                <h3>{this.props.myDataProp}</h3>
            </div>
        );
    }
}
export default Childevent;