import React from 'react';
import ReactDOM from 'react';
class Refsexample extends React.Component{
    constructor(props){
        super(props);
        this.state = {
            data:''
        }
        this.updateState = this.updateState.bind(this);
        this.clearInput = this.clearInput.bind(this);
    }// end of constructor
    updateState(e){
        this.setState({
            data:e.target.vale
        });
    }
    clearInput(){
        this.setState({data: ''});
        ReactDOM.findDOMNode(this.refs.myInput).focus();

    }
    render(){
        return(
            <div>
                <input value={this.state.data} 
                onChange = {this.updateState} 
                ref = "myInput"></input>
                <button onClick={this.clearInput}>clear</button>
                <h4>{this.state.data}</h4>
            </div>
        );
    }















}
export default Refsexample;