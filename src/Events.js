import React from 'react';
import ReactDOM from 'react';
class Events extends React.Component{
    constructor(props){
        super(props);
        this.state = {isToggleON:true

        };
        this.handleClick = this.handleClick.bind(this);

    }
    handleClick(){
        this.setState(prevState => ({
            isToggleON:!prevState.isToggleON

        }));
    }
    render()
    {
        return(
            <button onClick={this.handleClick}>
                {this.state.isToggleON ? 'ON' : 'OFF'}
            </button>       
             );
    }
}
export default Events;