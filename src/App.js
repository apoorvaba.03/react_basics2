import logo from './logo.svg';
import './App.css';
import React from 'react';
import ReactDom from 'react';

class App extends React.Component{
  constructor(props){
    super(props)
    this.state = {
      liked:false
    }
    this.handleClick = this.handleClick.bind(this)


  }
  handleClick(event){
    this.setState(
      {liked:!this.state.liked}
    )
  }
  render(){
    var text = this.state.liked ? 'like' : 'have not liked';
    return(
      <p onClick={this.handleClick}> 
        you {text} this. click to toggle.
      </p>
    );
  }
}

export default App;
