import React from 'react';
import ReactDOM from 'react-dom';
import './index.css';
import App from './App';
import reportWebVitals from './reportWebVitals';
import Statesexample from './Statesexample';
import Events from './Events';
import Childevent from './Childevent';
import Refsexample from './Refsexample';

ReactDOM.render(
  
  //  <App />
  //  <Statesexample />,
 // <Events />,
 <Childevent />,
 //<Refsexample />,
  document.getElementById('root')
);

// If you want to start measuring performance in your app, pass a function
// to log results (for example: reportWebVitals(console.log))
// or send to an analytics endpoint. Learn more: https://bit.ly/CRA-vitals
reportWebVitals();
